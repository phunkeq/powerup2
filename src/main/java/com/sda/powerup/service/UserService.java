package com.sda.powerup.service;


import com.sda.powerup.datasource.PasswordRecovery;
import com.sda.powerup.datasource.Role;
import com.sda.powerup.datasource.User;
import com.sda.powerup.dto.ResetPassword;
import com.sda.powerup.exception.FieldIsMandatoryException;
import com.sda.powerup.exception.UserNotFoundException;
import com.sda.powerup.repository.PasswordRecoveryRepository;
import com.sda.powerup.repository.RoleRepository;
import com.sda.powerup.repository.UserRepository;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private PasswordRecoveryRepository passwordRecoveryRepository;

    public User create(User user) {

        validate(user);

        long count = userRepository.countByEmail(user.getEmail());
        if (count > 0) {
            throw new RuntimeException("Email already in used");
        }
        long count2 = userRepository.countByUsername(user.getUsername());
        if (count2 > 0) {
            throw new RuntimeException("Username is already taken, please choose another one");
        }
        long count3 = userRepository.countByPhone(user.getPhone());
        if (user.getPhone().length() != 10) {
            throw new RuntimeException("Phone number is not valid");
        } else {
            if (count3 > 0) {
                throw new RuntimeException("Phone number is already in use, please provide a new one");
            }
        }

        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        return userRepository.save(user);
    }

    private void validate(User user) {
        if (Strings.isEmpty(user.getFirstName())) {
            throw new FieldIsMandatoryException("Please provide your First Name!");
        }
        if (Strings.isEmpty(user.getLastName())) {
            throw new FieldIsMandatoryException("Please provide your Last Name!");
        }
        if (Strings.isEmpty(user.getPhone())) {
            throw new FieldIsMandatoryException("Please provide your phone number!");
        }
        if (Strings.isEmpty(user.getGender())) {
            throw new FieldIsMandatoryException("Please state your gender!");
        }
        if (Strings.isEmpty(user.getEmail())) {
            throw new FieldIsMandatoryException("Please provide your email!");
        }
        if (Strings.isEmpty(user.getUsername())) {
            throw new FieldIsMandatoryException("Please provide a username!");
        }
        if (Strings.isEmpty(user.getAddress())) {
            throw new FieldIsMandatoryException("Please state your current address");
        }
    }

    public User update(Integer id, User user) {
        User dbUser = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException());
        dbUser.setFirstName(user.getPhone());
        dbUser.setLastName(user.getPhone());
        dbUser.setAddress(user.getAddress());
        return userRepository.save(dbUser);
    }

    public void delete(Integer id) {
        userRepository.deleteById(id);
    }


    public void promote(Integer id) {
        User dbUser = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException());

        Role role = new Role();
        role.setUserId((dbUser.getId()));
        role.setRole("Employee");

        roleRepository.save(role);
    }

    public String forgetPassword(String email) {

        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new RuntimeException("User not found");
        }

        String uuid = UUID.randomUUID().toString();
        PasswordRecovery passwordRecovery = new PasswordRecovery();
        passwordRecovery.setDate(new Date());
        passwordRecovery.setUserId(user.getId());
        passwordRecovery.setUid(uuid);

        passwordRecoveryRepository.save(passwordRecovery);

        return uuid;
    }

    public void resetPassword(ResetPassword resetPassword) {

        PasswordRecovery passwordRecovery =
                passwordRecoveryRepository.findByUid(resetPassword.getUid());

        if (passwordRecovery == null) {
            throw new RuntimeException("Bad request");
        }

        User user = userRepository.findById(passwordRecovery.getUserId()).get();
        if (user == null) {
            throw new RuntimeException("Bad request");
        }

        user.setPassword(bCryptPasswordEncoder.encode(resetPassword.getNewPassword()));
        userRepository.save(user);
        passwordRecoveryRepository.delete(passwordRecovery);

    }

}
