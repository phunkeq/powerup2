package com.sda.powerup.service;

import com.sda.powerup.datasource.Game;
import com.sda.powerup.datasource.Order;
import com.sda.powerup.datasource.User;
import com.sda.powerup.exception.GameNotFoundException;
import com.sda.powerup.repository.GameRepository;
import com.sda.powerup.repository.OrderRepository;
import com.sda.powerup.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService {

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderRepository orderRepository;


    public Order buy(com.sda.powerup.dto.Order order) {

        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        User currentUser = userRepository.findByEmail(userDetails.getUsername());

        Optional<Game> optionalGame = gameRepository.findById(order.getGameId());
        if (!optionalGame.isPresent()) {
            throw new GameNotFoundException();
        }

        Game game = optionalGame.get();

        Order entity = new Order();
        entity.setGameId(game.getId());
        entity.setUserId(currentUser.getId());
        entity.setDate(new Date());

        return orderRepository.save(entity);
    }

    public List<Order> findCurrentOrders() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        User currentUser = userRepository.findByUsername(userDetails.getUsername());

        return orderRepository.findByUserId(currentUser.getId());
    }

    public List<Order> findAll() {
        return orderRepository.findAll();
    }

}
