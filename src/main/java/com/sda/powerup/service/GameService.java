package com.sda.powerup.service;


import com.sda.powerup.datasource.Game;
import com.sda.powerup.exception.FieldIsMandatoryException;
import com.sda.powerup.repository.GameRepository;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GameService {

    @Autowired
    private GameRepository gameRepository;

    public List<Game> findAll(String name) {
        if (Strings.isEmpty(name)) {
            return gameRepository.findAll();
        }
        return gameRepository.findByName(name);
    }

    public Game create(Game game) {
        validate(game);
        return gameRepository.save(game);
    }

    private void validate(Game game) {
        if (Strings.isEmpty(game.getName())) {
            throw new FieldIsMandatoryException("Name is mandatory");
        }
        if (game.getPrice() == null) {
            throw new FieldIsMandatoryException("Price is mandatory");
        }
        if (Strings.isEmpty(game.getDescription())) {
            throw new FieldIsMandatoryException("Description is mandatory");
        }
        if (Strings.isEmpty(game.getPublisher())) {
            throw new FieldIsMandatoryException("Publisher is mandatory");
        }
        if (Strings.isEmpty(game.getPlatform())) {
            throw new FieldIsMandatoryException("Platform is mandatory");
        }
        if (Strings.isEmpty(game.getGameType())) {
            throw new FieldIsMandatoryException("The game type is mandatory");
        }
        if (game.getReleaseDate() == null) {
            throw new FieldIsMandatoryException("Release date is mandatory");
        }
    }

}
