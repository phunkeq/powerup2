package com.sda.powerup.controller;

import com.sda.powerup.datasource.Order;
import com.sda.powerup.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping
    public Order buy(@RequestBody com.sda.powerup.dto.Order order) {
        return orderService.buy(order);
    }

    @GetMapping("/findCurrentOrders")
    public List<Order> findCurrentOrders() {
        return orderService.findCurrentOrders();
    }

    @GetMapping("/findAll")
    public List<Order> findAll() {
        return orderService.findAll();
    }
}
