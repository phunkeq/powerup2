package com.sda.powerup.controller;

import com.sda.powerup.datasource.User;
import com.sda.powerup.dto.ResetPassword;
import com.sda.powerup.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/newUser", method = RequestMethod.POST)
    public User create(@RequestBody User user) {
        return userService.create(user);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public User update(
            @PathVariable(name = "id") Integer id,
            @RequestBody User user){
        return userService.update(id,user);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable(name = "id") Integer id) {
        userService.delete(id);
    }

    @RequestMapping(value = "/promote/{id}", method = RequestMethod.PUT)
    public void promote(
            @PathVariable(name = "id") Integer id){
        userService.promote(id);
    }

    @PostMapping("/forgetPassword/{email}")
    public String forgetPassword(@PathVariable(name = "email") String email) {
        return userService.forgetPassword(email);
    }

    @PostMapping("/resetPassword")
    private void resetPassword(@RequestBody ResetPassword resetPassword) {
        userService.resetPassword(resetPassword);
    }


}
