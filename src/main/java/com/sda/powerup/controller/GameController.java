package com.sda.powerup.controller;


import com.sda.powerup.datasource.Game;
import com.sda.powerup.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/games")
public class GameController {

    @Autowired
    private GameService gameService;

    @GetMapping("/findAll")
    public List<Game> findAll(@RequestParam(name = "name", required = false)
                                      String name) {
        return gameService.findAll(name);
    }

    @PostMapping("/newGame")
    public Game create(@RequestBody Game game) {
        return gameService.create(game);
    }
    




}
