package com.sda.powerup.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApplicationController {

    @GetMapping("/home")
    public String home() {
        return "Great Job! You've done the easiest thing so far, now chop chop, do the other things as well";
    }
}

