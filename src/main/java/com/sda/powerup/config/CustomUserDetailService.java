package com.sda.powerup.config;


import com.sda.powerup.datasource.Role;
import com.sda.powerup.datasource.User;
import com.sda.powerup.repository.RoleRepository;
import com.sda.powerup.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        List<String> roleStringList = new ArrayList<>();

        List<Role> roles = roleRepository.findByUserId(user.getId());

        if (roles.isEmpty()) {
            roleStringList.add("USER");
        } else {
            for (Role role : roles){
                roleStringList.add(role.getRole());
            }
        }
        return org.springframework.security.core.userdetails.User
                .withUsername(user.getUsername())
                .username(user.getUsername())
                .password(user.getPassword())
                .roles(roleStringList.stream().collect(Collectors.joining()))
                .build();
    }
}
