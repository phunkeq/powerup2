package com.sda.powerup.repository;


import com.sda.powerup.datasource.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

    List<Role> findByUserId(Integer userId);
}
