package com.sda.powerup.repository;

import com.sda.powerup.datasource.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    User findByEmail(String email);

    User findByUsername(String username);

    long countByEmail(String email);

    long countByUsername(String username);

    long countByPhone(String phone);
}
