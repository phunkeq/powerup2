package com.sda.powerup.repository;

import com.sda.powerup.datasource.Game;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GameRepository extends JpaRepository<Game, Integer> {
List<Game> findByName(String name);

}
