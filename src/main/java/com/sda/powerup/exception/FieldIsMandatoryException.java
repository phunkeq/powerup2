package com.sda.powerup.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "This field is mandatory")
public class FieldIsMandatoryException extends RuntimeException {

    public FieldIsMandatoryException(String msg){
        super(msg);
    }
}
